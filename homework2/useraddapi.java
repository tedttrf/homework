package com.max.slw.api;

import cn.hutool.json.JSONUtil;
import com.max.slw.dao.DemoDao;
import com.max.slw.dao.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "useraddapi", urlPatterns = "/user/add")
public class useraddapi extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //super.doGet(req, resp);
        doHttp(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //super.doPost(req, resp);
        doHttp(req,resp);
    }

    private void doHttp(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String name=req.getParameter("name");

        DemoDao dao=new DemoDao();
        User user = new User();
        user.setName(name);
        int res = dao.insert(user);
        boolean blres = (res==0?false:true);
        String json = JSONUtil.toJsonStr(blres);


        resp.setContentType("application/json;charset=utf-8");
        PrintWriter pw = resp.getWriter();
        pw.write(json);
        pw.flush();
    }
}
